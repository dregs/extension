function checkIframeLoaded() {
    var iframe = document.getElementById("chatframe");
    if (iframe != null) {
        var iframeDoc = iframe.contentWindow.document;

        if (  iframeDoc.readyState  == 'complete' ) {
            iframe.contentWindow.onload = function(){
                console.info("chat frame found");
            };
            checkMonitorLoaded();
            return;
        }
    }
    window.setTimeout(checkIframeLoaded, 100);
}

function checkMonitorLoaded() {
    var frame = document.getElementById("chatframe");
    var x = frame.contentDocument.getElementById('visibility-monitor');
    console.info("looking for visibility monitor...");
    if (x != null) {
        x.innerHTML = '<style scope="yt-live-chat-text-message-renderer"> yt-live-chat-text-message-renderer[is-deleted]:not([show-original]) #message.yt-live-chat-text-message-renderer{ display:block !important; color:red !important; font-weight:bold !important; }</style>';
        console.log(x);
        return;
    }
    window.setTimeout(checkMonitorLoaded, 100);
}

window.onload = function() {

    checkIframeLoaded();

}
