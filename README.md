# AwfulChat
Tired of Google's [nooglers](https://www.lifewire.com/xooglers-nooglers-1616985) censoring your chats in Youtube livestreams? Well fret no more! Pop AwfulChat onto your browser and prepare to experience the full and terrible hell world that is Youtube Livechat!  AwfulChat: illuminating your journey into the #DarkFuture! 

## What is AwfulChat?
AwfulChat is a small extension that does carries out two basic functions:

 1. Stops Youtube's censorbots from hiding naughty comments in livestream chats.
 2. Conveniently and spitefully highlights the messages Google's Moderation Team thought you shouldn't see in a **bold** and *sumptuous* red.

## How do I AwfulChat?

 1. Download the `awfulchat.crx` file
 2. Open Chrome's `Extensions` tab by going through the 3-dot menu in the upper right corner or just go to [chrome://extensions/](chrome://extensions/) in a new tab.
 ![Step 2](img/step2.png)
 
 3. Switch Developer Mode on using the switch in the top right of the header. This allows you to load custom extensions like AwfulChat. After installation you can switch this back to off if you'd like.
 ![Step 3](img/step3.png)
 
 4. Drag and drop `awfulchat.crx` onto the `Extensions` tab. Read the text, and approve that shit. We gotta read the data on the Youtube Page to do the decensoring magic.
 ![Step 3](img/step4.png)
 5. Go to your favorite Youtuber's livestream and enjoy the unfiltered carnage! 

## Why Awfulchat?

On Tuedsay, October 16th, 2018 Youtube went down for just about everyone around the globe for a little more than an hour, and around the globe people squeeled with displeasure at the thought of not being able to watch Pewdiepie's newest Meme Review or catch up on their favorite vegan mommy-vlogger's trip to ~~thirdworld countries~~ delveloping nations via sailboat. 

The very next day, sometime around 4:20pm EST people started reporting that their naughty words and no-no thoughts were being censored from livestream chats on the platform. They would be deleted and replaced by `[message deleted]` from what the [viewers could see](https://twitter.com/zyntrax/status/1052661872414117888). Streamers would see a new message; [message deleted by Google Moderation Team](https://twitter.com/zyntrax/status/1052657767725629441). 

Chaos ensured. "Thanks  skeptic community! Hope you liked that Candid money" echoed through Twitter. While everyone had a fit, there sat a few of dregs conspired in basements spread across the country, connected by their AU gold and the wonders of the internet.  Throughout the night they worked to bring to the people a lantern to shine light on all of the wonderfully awful things livechat has to offer.  

And here we are today. Enjoy. Byeeeeeeeeeeeeee.
 
### Brought to you by the kind and gentle people at Pod Awful� 
###### I'm in that ass, bro.

# For educational and entertainment purposes only.